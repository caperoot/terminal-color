## About
This is a script to change the color scheme of urxvt terminal. The script first changes `.Xdefaults` file in the home folder, and then in the second part reflects the changes on current instance of urxvt. 

---

## Usage
Copy the folder `terminal-color` to `~/.config/` and then check the contents of file `urxvt_settings` and edit or add to the settings. Next run the `terminal-color` script.

```
cd ~/.config/
git clone https://caperoot@bitbucket.org/caperoot/terminal-color.git
cd terminal-color
./terminal-color
```
---


